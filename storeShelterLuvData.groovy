import groovy.json.JsonSlurper
import groovy.transform.Field
import com.onresolve.scriptrunner.db.DatabaseUtil

//########## CONSTANTS ##########
@Field final String SHELTERLUVAPIKEY = "REDACTED"

//DB table names
@Field final String PEOPLE_TABLE = "scriptrunner_shelterluv_people"
@Field final String ANIMAL_TABLE = "scriptrunner_shelterluv_animal"
@Field final String BRIDGE_TABLE = "scriptrunner_shelterluv_people_animal"

//API URLs:

@Field final String ANIMAL_URL = "https://shelterluv.com/api/v1/animals"
@Field final String PEOPLE_URL = "https://shelterluv.com/api/v1/people"

//######## END CONSTANTS ########

private boolean checkDB()
{
    DatabaseUtil.withSql('REDACTED') { sql ->

        def create_table_people = """
  CREATE TABLE IF NOT EXISTS """+PEOPLE_TABLE+""" (
    internal_id   INT,
    id          VARCHAR(32),
    firstname   VARCHAR(64),
    lastname    VARCHAR(64),
    email VARCHAR(128),
    PRIMARY KEY (internal_id)
  );"""
        def create_table_animal = """
  CREATE TABLE IF NOT EXISTS """+ANIMAL_TABLE+""" (
    internal_id          INT,
    id     VARCHAR(32),
    name   VARCHAR(128),
    sex    VARCHAR(32),
    type   VARCHAR(32),
    cover_photo VARCHAR(256),
    PRIMARY KEY (internal_id)
  );"""

        def create_table_bridge = """
  CREATE TABLE IF NOT EXISTS """+BRIDGE_TABLE+""" (
    person_id   INT,
    animal_id   INT,
    PRIMARY KEY (person_id, animal_id),
    FOREIGN KEY (person_id) REFERENCES scriptrunner_shelterluv_people(internal_id) ON DELETE CASCADE,
    FOREIGN KEY (animal_id) REFERENCES scriptrunner_shelterluv_animal(internal_id) ON DELETE CASCADE
  );
"""


        /**
         * For future updates:
         * IF NOT EXISTS( SELECT NULL
         FROM INFORMATION_SCHEMA.COLUMNS
         WHERE table_name = 'tablename'
         AND table_schema = 'db_name'
         AND column_name = 'columnname')  THEN

         ALTER TABLE `TableName` ADD `ColumnName` int(1) NOT NULL default '0';**/

        def create_results_people = sql.execute create_table_people;
        def create_results_animal = sql.execute create_table_animal;
        def create_results_bridge = sql.execute create_table_bridge;

        List results = sql.rows('show tables like "scriptrunner_shelterluv%"');
        assert results.size() == 3;
        sql.close()
    }
    return true;
}

private List<Map> makePagedAPICall(String url, pageLimit)
{
    boolean moreToProcess = true;
    int offset = 0;
    def List<Map> resultList = [];

    while(moreToProcess)
    {
        def Map apiResults = getJsonResults("$url?limit=$pageLimit&offset=$offset");
        assert apiResults["success"] == 1;
        resultList += apiResults[url.substring(url.lastIndexOf('/')+1)] as Map;

        if(!apiResults["has_more"])
        {
            moreToProcess = false;
        }
        else
        {
            offset = offset+100;
        }
    }
    return resultList;
}




private updateShelterLuvTables(List<Map> peopleList, List<Map> animalList)
{
    Map personToAnimals = [:];

    DatabaseUtil.withSql('Jira DB Server') { sql ->

        //people
        sql.withTransaction {

            sql.withBatch(20, "REPLACE INTO "+PEOPLE_TABLE+" VALUES (?, ?, ?, ?, ?)") {
                ps ->
                    peopleList.each
                            { person ->
                                def internal_id = person["Internal-ID"];
                                def id = person["ID"];
                                def fname = person["Firstname"];
                                def lname = person["Lastname"];
                                def email = person["Email"];
                                def animals = person["Animal_ids"];

                                def v = [internal_id,id,fname,lname,email];
                                ps.addBatch(v);
                                personToAnimals.put person["Internal-ID"],person["Animal_ids"]
                            }
            }
        }


        //animals
        /**    internal_id          INT,
         id     VARCHAR(32),
         name   VARCHAR(128),
         sex    VARCHAR(32),
         type   VARCHAR(32),
         cover_photo VARCHAR(256),**/
        sql.withTransaction {
            sql.withBatch(20, "REPLACE INTO "+ANIMAL_TABLE+" VALUES (?, ?, ?, ?, ?, ?)") {
                ps ->
                    animalList.each
                            { animal ->
                                def internal_id = animal["Internal-ID"];
                                def id = animal["ID"];
                                def name = animal["Name"];
                                def sex = animal["Sex"];
                                def type = animal["Type"];
                                def cover_photo = animal["CoverPhoto"];

                                def v = [internal_id,id,name,sex,type,cover_photo];
                                ps.addBatch(v);
                            }
            }
        }

        //bridge
        sql.withTransaction {
            //Wipe the bridge table - as we don't want stale animal associations
            sql.execute "DELETE FROM "+BRIDGE_TABLE+";"
            sql.withBatch(20, "INSERT IGNORE INTO "+BRIDGE_TABLE+" VALUES (?, ?)") {
                ps ->
                    personToAnimals.each
                            { person_id,animal_list ->
                                animal_list.each{
                                    animal_id ->
                                        def v = [person_id, animal_id];
                                        ps.addBatch(v);
                                }
                            }
            }
        }
        sql.close()
    }
}

Map getJsonResults(String url) {
    def connection = new URL(url).openConnection() as HttpURLConnection
    connection.setRequestMethod( "GET" )
    connection.setRequestProperty( "X-Api-Key", SHELTERLUVAPIKEY )
    connection.doOutput = true
    connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
    connection.connect()
    def postRC = connection.getResponseCode();
    def response;

    if(postRC.equals(200)) {
        response = connection.getInputStream().getText();
    }

    //log.info response;
    def jsonSlurper = new JsonSlurper()


    def results = jsonSlurper.parseText(response);

    assert results != null

    return results as Map
}

log.debug "Checking DB Schema and creating/updating tables as necessary"
checkDB();
log.debug "DB check done"
log.debug "Beginning update of tables"
updateShelterLuvTables(makePagedAPICall(PEOPLE_URL,100) as List<Map>,makePagedAPICall(ANIMAL_URL,100) as List<Map>);
log.debug "Job's done!"
