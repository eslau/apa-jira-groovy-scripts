import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.*
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.event.type.EventDispatchOption
import groovy.transform.Field
import com.onresolve.scriptrunner.db.DatabaseUtil


//########## CONSTANTS ##########
@Field final String SHELTERLUVDATA_FIELD = "ShelterLuv Data"

// change to 'true' if you want to send an email if the update is successful
@Field final boolean SENDMAIL = false

//DB table names
@Field final String PEOPLE_TABLE = "scriptrunner_shelterluv_people"
@Field final String ANIMAL_TABLE = "scriptrunner_shelterluv_animal"
@Field final String BRIDGE_TABLE = "scriptrunner_shelterluv_people_animal"

//Project Info
//@todo: turn this from a string to a list of different projects
//       or maybe just declare ISSUE_JQL as a constant and roll from there
@Field final String JIRA_PROJECT = "CATBEHAV"

//######## END CONSTANTS ########

/**
 * @return Map of issue key to reporter email address
 */
def Map getIssueEmails(searchResults) {
    def issueManager = ComponentAccessor.getIssueManager()
    Map returnMap = [:]
    searchResults.each { documentIssue ->
        log.debug(documentIssue.key)
        def issue = issueManager.getIssueObject(documentIssue.id)
        returnMap.put documentIssue.key, issue.reporter.getEmailAddress()
    }
    return returnMap
}


private Map getAnimalsForEmails(issueEmails) {
    def emailAnimalMap = [:].withDefault { [] }
    DatabaseUtil.withSql('Jira DB Server') { sql ->
        def select_animals_for_email = """
  SELECT p.email,a.internal_id,a.id,a.sex,a.name,a.type,a.cover_photo FROM """ + PEOPLE_TABLE + """ p 
  INNER JOIN """ + BRIDGE_TABLE + """ b 
    ON p.internal_id=b.person_id 
  INNER JOIN """ + ANIMAL_TABLE + """ a
    ON a.internal_id=b.animal_id
  WHERE p.email in ("""



        def emailList = issueEmails.values().unique(false)

        emailList.each{
            email ->
                select_animals_for_email+="\'${email}\',"
        }
        select_animals_for_email = select_animals_for_email.substring(0, select_animals_for_email.length() - 1) + ");"
        sql.eachRow(select_animals_for_email) { row ->
            def animal = [:]
            animal["internal_id"] = row.internal_id
            animal["id"] = row.id
            animal["sex"] = row.sex
            animal["name"] = row.name
            animal["type"] = row.type
            animal["cover_photo"] = row.cover_photo

            emailAnimalMap[row.email].add(animal)
        }

        /** def issueAnimalMap = [:].withDefault {[]}issueEmails.each { issueKey,email ->
         issueAnimalMap[issueKey].add(emailAnimalMap[email])}returnMap['issueAnimalMap'] = issueAnimalMap
         returnMap['emailAnimalMap'] = emailAnimalMap
         */



        sql.close()
    }

    return emailAnimalMap
}

private void updateIssueFields(Map issueEmails, Map emailAnimals) {
    issueEmails.each
            {
                issueKey, email ->
                    def hasAnimals = emailAnimals.containsKey(email)
                    if (hasAnimals) {
                        def shelterLuvAnimalString = """h3. Animals for """+email+"""
            
            
            """
                        emailAnimals[email].each
                                { animal ->
                                    def name = animal["name"]
                                    def internal_id = animal["internal_id"]
                                    def id = animal["id"]
                                    def sex = animal["sex"]
                                    def type = animal["type"]
                                    def coverPhoto = animal["cover_photo"]
                                    shelterLuvAnimalString += """h4. ${name}
            
            !${coverPhoto}|height=100,width=100!
            
            * Type: ${type}
            * Sex: ${sex}
            * ID: ${id}
            * [Link to ShelterLuv Animal Profile|https://www.shelterluv.com/${id}]
            
            """
                                }
                        //update ShelterLuv Data Field with animal data
                        updateSingleCustomFieldByName(SHELTERLUVDATA_FIELD, shelterLuvAnimalString, issueKey)
                        log.info "Set ShelterLuv data for issue  " + issueKey
                    } else {
                        //update to say that they're not in shelterluv
                        updateSingleCustomFieldByName(SHELTERLUVDATA_FIELD, "No data found in ShelterLuv for email  " + email, issueKey)
                        log.info "Set no data found value for issue  " + issueKey

                        //TODO: check APA ID too
                        onlyOneForNow = true;
                    }
            }
}

boolean updateSingleCustomFieldByName(String fieldName, String value, String issueKey) {
    def issueService = ComponentAccessor.issueService
    def loggedInUser = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def issue = ComponentAccessor.issueManager.getIssueByCurrentKey(issueKey as String)


    def issueInputParameters = issueService.newIssueInputParameters().with {
        addCustomFieldValue(getSingleCustomFieldByName(fieldName, issueKey).id, value)
    }

    def updateValidationResult = issueService.validateUpdate(loggedInUser, issue.id, issueInputParameters)
    assert updateValidationResult.isValid(): updateValidationResult.errorCollection

    def issueUpdateResult = issueService.update(loggedInUser, updateValidationResult, EventDispatchOption.ISSUE_UPDATED, SENDMAIL)
    assert issueUpdateResult.isValid(): issueUpdateResult.errorCollection
    return issueUpdateResult.isValid();
}

/**
 * Get a custom field given a custom field name.
 * If there are than one custom fields with the same name under the same Context then return the first one.
 * @param fieldName The name of the custom field
 * @param issue The issue to look for that custom field
 * @return the custom field, if that exists
 */
CustomField getSingleCustomFieldByName(String fieldName, String issueKey) {
    def issue = ComponentAccessor.issueManager.getIssueByCurrentKey(issueKey)
    def customField = ComponentAccessor.customFieldManager.getCustomFieldObjects(issue).findByName(fieldName)

    assert customField: "Could not find custom field with name $fieldName"
    customField
}

/**
 * Given a custom field name and option values, retrieve their ids as String
 * @param customFieldName The name of the custom field
 * @param values The values in order to get their ids
 * @return List < String >  The ids of the given values
 */
List<String> getOptionIdsForFieldByValue(String customFieldName, String... values) {
    def issue = ComponentAccessor.issueManager.getIssueByCurrentKey(issueKey)
    def customField = getSingleCustomFieldByName(customFieldName)

    ComponentAccessor.optionsManager.getOptions(customField.getRelevantConfig(issue)).findAll {
        it.value in values.toList()
    }*.optionId*.toString()
}


def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
def searchService = ComponentAccessor.getComponent(SearchService)
def user = ComponentAccessor.getJiraAuthenticationContext().getUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

// get tickets with no shelterluv data
def query = jqlQueryParser.parseQuery("project = " + JIRA_PROJECT + " AND \"" + SHELTERLUVDATA_FIELD + "\" is EMPTY ")
def search = searchService.search(user, query, PagerFilter.getUnlimitedFilter())
log.debug("Total issues: ${search.total}")
if(search.total > 0) {
    def Map issueEmails = getIssueEmails(search.results)

    def Map emailAnimals = getAnimalsForEmails(issueEmails)

    log.info "Animal list compiled";

//update tickets

    updateIssueFields(issueEmails, emailAnimals)
}
else
{
    log.info "Nothing to update!"
}
log.info "Job's done!"