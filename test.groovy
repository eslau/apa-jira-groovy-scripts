import groovy.json.JsonSlurper
import groovy.transform.Field



//########## CONSTANTS ##########
@Field final String SHELTERLUVAPIKEY = "4c925183-2bb9-40bb-9cd2-b62459a84f2e"

//DB table names
@Field final String PEOPLE_TABLE = "scriptrunner_shelterluv_people"
@Field final String ANIMAL_TABLE = "scriptrunner_shelterluv_animal"
@Field final String BRIDGE_TABLE = "scriptrunner_shelterluv_people_animal"


def create_table_people = """
  CREATE TABLE IF NOT EXISTS $PEOPLE_TABLE (
    internal_id   INT,
    id          VARCHAR(32),
    firstname   VARCHAR(64),
    lastname    VARCHAR(64),
    email VARCHAR(128),
    PRIMARY KEY (internal_id)
  );"""

println create_table_people